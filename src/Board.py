import pygame

pygame.init()

MAROON = (250,235,215)
WHITE = (255,255,255)
BLACK = (0,0,0)
LINE_COL =  (178,34,34)
CIR_COL = (26,24,25)

points = [(375,65),(100,700),(100,700),(650,700),(283,700),(466,700),(50,560),(700,560),
(50,420),(700,420),(50,280),(700,280),(50,560),(700,560),(700,560),(278,280),(345,280),(410,280),(480,280),
(215,420),(320,420),(430,420),(530,420),(158,562),(305,559),(447,559),(595,561)]

def drawPoints(radius):
	for pt in points:
		pygame.draw.circle(screen,CIR_COL,pt,radius,0)

def boardPolygons(screen,LINE_COL,CIR_COL):
	pygame.draw.line(screen,LINE_COL,(100,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(650,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(100,700),(650,700),10)
	pygame.draw.line(screen,LINE_COL,(283,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(466,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(50,560),(700,560),10)
	pygame.draw.line(screen,LINE_COL,(50,420),(700,420),10)
	pygame.draw.line(screen,LINE_COL,(50,280),(700,280),10)
	pygame.draw.line(screen,LINE_COL,(50,280),(50,560),10)
	pygame.draw.line(screen,LINE_COL,(700,280),(700,560),10)
	drawPoints(17)
def tigerImage():
	tiger = pygame.image.load("/home/meghana/Downloads/tiger3.bmp").convert()
	screen.blit(tiger,[360,40])
	screen.blit(tiger,[320,264])
	screen.blit(tiger,[390,264])
	tiger.set_colorkey(BLACK)
def load_background():
	background_image = pygame.image.load("/home/meghana/Downloads/pic1.bmp").convert()
	screen.blit(background_image,[0,0])
	
	
size = [1000,950]
screen = pygame.display.set_mode(size)
pygame.display.set_caption("AADU PULI AATAM")
done = False
clock = pygame.time.Clock()

while not done:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			done = True
		elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
			pos = pygame.mouse.get_pos()
			print pos

	pygame.display.flip()
	screen.fill(MAROON)
	load_background()
	clock.tick(60)
	boardPolygons(screen,LINE_COL,CIR_COL)
	tigerImage()

	
pygame.quit()