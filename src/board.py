import pygame

pygame.init()

WHITE = ( 255, 255,255)
LINE_COL =  (178,34,34)
CIR_COL = (26,24,25)
POINTS = [[375,65],[50,280],[278,280],[345,280],[410,280],[480,280],[700,280],[50,420],[215,420],[320,420],[430,420],[530,420],[700,420],[50,560],[158,560],[305,560],[450,560],[595,560],[700,560],[100,700],[283,700],[466,700],[650,700]]
ADJACENCY = [
[-1,0,1,1,1,1,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0],
[0,-1,1,2,0,0,0,1,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0],
[1,1,-1,1,2,0,0,0,1,0,0,0,0,0,2,0,0,0,0,0,0,0,0],
[1,2,1,-1,1,2,0,0,0,1,0,0,0,0,0,2,0,0,0,0,0,0,0],
[1,0,2,1,-1,1,2,0,0,0,1,0,0,0,0,0,2,0,0,0,0,0,0],
[1,0,0,2,1,-1,1,0,0,0,0,1,0,0,0,0,0,2,0,0,0,0,0],
[0,0,0,0,2,1,-1,0,0,0,0,0,1,0,0,0,0,0,2,0,0,0,0],
[0,1,0,0,0,0,0,-1,1,2,0,0,0,1,0,0,0,0,0,0,0,0,0],
[2,0,1,0,0,0,0,1,-1,1,2,0,0,0,1,0,0,0,0,2,0,0,0],
[2,0,0,1,0,0,0,2,1,-1,1,2,0,0,0,1,0,0,0,0,2,0,0],
[2,0,0,0,1,0,0,0,2,1,-1,1,2,0,0,0,1,0,0,0,0,2,0],
[2,0,0,0,0,1,0,0,0,2,1,-1,1,0,0,0,0,1,0,0,0,0,2],
[0,0,0,0,0,0,1,0,0,0,2,1,-1,0,0,0,0,0,1,0,0,0,0],
[0,2,0,0,0,0,0,1,0,0,0,0,0,-1,1,2,0,0,0,0,0,0,0],
[0,0,2,0,0,0,0,0,1,0,0,0,0,1,-1,1,2,0,0,1,0,0,0],
[0,0,0,2,0,0,0,0,0,1,0,0,0,2,1,-1,1,2,0,0,1,0,0],
[0,0,0,0,2,0,0,0,0,0,1,0,0,0,2,1,-1,1,2,0,0,1,0],
[0,0,0,0,0,2,0,0,0,0,0,1,0,0,0,2,1,-1,1,0,0,0,1],
[0,0,0,0,0,0,2,0,0,0,0,0,1,0,0,0,2,1,-1,0,0,0,0],
[0,0,0,0,0,0,0,0,2,0,0,0,0,0,1,0,0,0,0,-1,1,2,0],
[0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,1,0,0,0,1,-1,1,2],
[0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,1,0,0,2,1,-1,1],
[0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,1,0,0,2,1,-1]]
goats= ['G1','G2','G3','G4','G5','G6','G7','G8','G9','G10','G11','G12','G13','G14','G15']
tigers = ['T1','T2','T3']
JUMPS = {(0,8):2,(0,9):3,(0,10):4,(0,11):5,(1,3):2,(1,13):7,(2,4):3,(2,14):8,
(3,5):4,(3,15):9,(4,6):5,(4,16):10,(5,17):11,(6,18):12,(7,9):8,(8,10):9,
(8,19):14,(9,20):15,(9,11):10,(10,21):16,(10,12):11,(11,22):17,(13,15):14,
(14,16):15,(15,17):16,(16,18):17,(19,21):20,(20,22):21}
dic = {}

size = (1150, 1000)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Aadu Puli Aattam")

def generatePossiblePoints(): #all the possible points around every circle in the game
	for pt in POINTS:
		for a in range(pt[0] - 17, pt[0] + 18):
			for b in range(pt[1] - 17, pt[1] + 18):
				dic[a,b] = POINTS.index(pt)

#def PositionOfTheClick(pos) :
#	for pt in POINTS:
#		if pos[0] in range(pt[0] - 20,pt[0]+20) and pos[1] in range(pt[1] -20,pt[1] + 20):
#			return POINTS.index(pt)
#	return -1
	
def boardPolygons():
	pygame.draw.line(screen,LINE_COL,(100,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(650,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(100,700),(650,700),10)
	pygame.draw.line(screen,LINE_COL,(283,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(466,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(50,560),(700,560),10)
	pygame.draw.line(screen,LINE_COL,(50,420),(700,420),10)
	pygame.draw.line(screen,LINE_COL,(50,280),(700,280),10)
	pygame.draw.line(screen,LINE_COL,(50,280),(50,560),10)
	pygame.draw.line(screen,LINE_COL,(700,280),(700,560),10)
	drawPoints()
	
def drawPoints():
	for pt in POINTS:
		pygame.draw.circle(screen,CIR_COL,pt,17,0)

def loadPlayField():
	background_image = pygame.image.load("pic1.bmp").convert()
	screen.blit(background_image, [0, 0])
	boardPolygons()

def goatImage(move): #to place all 15 goats on the board
	move = 30 - move
	goat = pygame.image.load("gig.bmp").convert()
	space = 70
	lis = [[813,283],[813,283 + space],[813,283 + (2 * space)],[813,283+(3*space)],[884,283],[884,283 + space],[884,283 + (2 * space)],[884,283+(3*space)],[959,283],[959,283+space],[959,283+(2*space)],[959,283+(3*space)],[1034,283],[1034,283+space],[1034,283+(2*space)]]
	for point in lis:
		if move != 0:
			screen.blit(goat,point)
			move -= 2
		
def loadAnimals(move): # To place animals on board according to the dataList
	tiger = pygame.image.load("tigs.bmp").convert()
	goat = pygame.image.load("gig.bmp").convert()
	for animal in data:	
		if animal == 'T1' or animal == 'T2' or animal == 'T3':
			screen.blit(tiger,[POINTS[data.index(animal)][0] -17 , POINTS[data.index(animal)][1] - 17])
		elif animal == 'G1' or animal == 'G2' or animal == 'G3' or animal == 'G4' or animal == 'G5' or animal == 'G6' or animal == 'G7' or animal == 'G8' or animal == 'G9' or animal == 'G10' or animal == 'G11' or animal == 'G12' or animal == 'G13' or animal == 'G14' or animal == 'G15' :
			screen.blit(goat,[POINTS[data.index(animal)][0] - 17 , POINTS[data.index(animal)][1] - 17])
	if move in range(0,29):
		goatImage(move)
		
def isSpot(pos):
	print "entered isSpot()"
	print pos in dic
	print dic[pos]
	return pos in dic and data[dic[pos]] == 0
	
def isAnimal(pos,turn):
	print "entered isAnimal()"
	if pos in dic :
		if turn == 'T':
			return data[dic[pos]] == 'T1' or data[dic[pos]] == 'T2'  or data[dic[pos]] == 'T3'
		return (data[dic[pos]] == 'G1' or data[dic[pos]] == 'G2' or data[dic[pos]] == 'G3' or data[dic[pos]] == 'G4' or data[dic[pos]] == 'G5' or data[dic[pos]] == 'G6' or data[dic[pos]] == 'G7' or data[dic[pos]] == 'G8' or data[dic[pos]] == 'G9' or data[dic[pos]] == 'G10' or data[dic[pos]] == 'G11' or data[dic[pos]] == 'G12' or data[dic[pos]] == 'G13' or data[dic[pos]] == 'G14' or data[dic[pos]] == 'G15') and move >= 0
	elif  move < 30 and turn == 'G':
		return (pos[0] in range(813,1041)) and (pos[1] in range(284, 500))
	return False
	
def turnOf():#move
	if move % 2 == 0:
		return 'G'
	return 'T'
	
def isTigerBlocked():
	for t in tigers:
		for i in range(0,23):
			if (ADJACENCY[(data.index(t))][i] == 1 or ADJACENCY[(data.index(t))][i] == 2 ) and data[i] == 0:
				return False
	return True
	
def isGoatKilled():
	return len(killedGoats) == 15
	
	
def validJump(sourcePos, destinationPos):
	pos = JUMPS[dic[sourcePos], dic[destinationPos]]
	if data[pos] == 'G1' or data[pos] == 'G2' or  data[pos] == 'G3' or data[pos] == 'G4' or data[pos] == 'G5' or data[pos] == 'G6' or data[pos] == 'G7' or data[pos] == 'G8' or data[pos] == 'G9' or data[pos] == 'G10' or data[pos] == 'G11' or data[pos] == 'G12' or data[pos] == 'G13' or data[pos] == 'G14' or data[pos] == 'G15':
		killedGoats.append(data[pos])
		data[pos] = 0
		data[dic[destinationPos]] = data[dic[sourcePos]] 
		data[dic[sourcePos]] = 0
		move += 1
		return True
	return False
	
def validation(sourcePos, destinationPos):
	print "entered validation function"
	print turnOf(),
	print isSpot(destinationPos)
	print sourcePos , destinationPos
	global move 
	if isSpot(destinationPos) and isAnimal(sourcePos,turnOf()) and data[dic[destinationPos]] == 0: # pos's are validated
		#validating the click using adjList data list
		print turnOf(), "pos validated"
		if turnOf() == 'T' :
			if  ADJACENCY[dic[sourcePos]][dic[destinationPos]] == 1 :
				data[dic[destinationPos]] = data[dic[sourcePos]] 
				data[dic[sourcePos]] = 0	
				move += 1
			elif ADJACENCY[dic[sourcePos]][destinationPos] == 2 :
				validJump(sourcePos, destinationPos)
			isGoatsKilled()
		else :
			if move < 30: #goats are out of the board
				if data[dic[destinationPos]] == 0:
					data[dic[destinationPos]] = goats[move / 2]
					move += 1
			else: # inside the board
				if  ADJACENCY[dic[sourcePos]][dic[destinationPos]] == 1 :
					data[dic[destinationPos]] = data[dic[sourcePos]] 
					data[dic[sourcePos]] = 0	
					move += 1
			isTigerBlocked()
	del stack[0]
	del stack[0]
		
killedGoats = []	
stack = []
data = ['T1',0,0,'T2','T3',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]			
generatePossiblePoints()
done = False
clock = pygame.time.Clock()
move = 0
while not done:
	for event in pygame.event.get(): 
		if event.type == pygame.QUIT: 
			done = True 
		elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
			pos = pygame.mouse.get_pos()
			print pos
			stack.append(pos)
			if len(stack) == 2:
				print "2 pos captured"
				validation(stack[0],stack[1])
				screen.fill(WHITE)
		loadPlayField()
		#Play - Field
		loadAnimals(move)
		pygame.display.flip()
		clock.tick(20)

pygame.quit()