++++++++++++++++
AADU PULLI AATAM
++++++++++++++++

**Description:**

This game is a strategic, two-player (or 2 teams) board game. This is mostly played in south India. One player controls three tigers(pulis) and the other player controls 15 goats(aadus).
	
**Objective:**

The Tiger hunt the goats while the goat attempt to block the tiger movements.

**Rules for Goats(aadus):** ::

	1. They have to leave the board when ever they are captured.

	2. They cannot jump over other goats or tigers

	3. They are supposed to move only when all 15 goats are placed.

**Rules for Tigers(Pulis):** ::

	1. They can start capturing as the game starts.

	2. They can jump over goats but not over other tigerss.

	3. They can capture only one goat at a time.

	4. They can jump in any direction, but only on to the adjacent intersection of lines drawn on board.


**Tools:** ::

	1. Pygame.
	
	2. Python.
