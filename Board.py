import pygame

pygame.init()

MAROON = (250,235,215)
WHITE = (255,255,255)
BLACK = (0,0,0)
LINE_COL =  (178,34,34)
CIR_COL = (26,24,25)

list = ['0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0']
points = [[375,65],[50,280],[278,280],[345,280],[480,280],[700,280],[50,420],[215,420],[320,420],[430,420],[530,420],[700,420],[50,560],[158,560],[305,560],[477,560],[595,560],[700,560],[100,700],[283,700],[466,700],[650,700]]
pos = 0
center = 0
dic = {}
count = -1
def validPosition(pos):
	for key in dic:
		if pos == key:
			return dic[key]
		return -1
	
def isPictureOrSpace(pos): #it is for knowing whether the clicked point is on the picture or on the circle or somewhere which is not valid
	goat = pygame.image.load("/home/bvrit/Desktop/game/gig.bmp").convert()
	tiger = pygame.image.load("/home/bvrit/Desktop/game/tigs.bmp").convert()
	if validPosition(pos):
		center = validPosition(pos)
		if list[points[center]] != '0':
			tiger.kill()
	
	
	
def generatePossiblePoints(): #all the possible points around every circle in the game
	for pt in points:
		for a in range(pt[0] - 17, pt[0] + 18):
			for b in range(pt[1] - 17, pt[1] + 18):
				dic[a,b] = pt
				
def changePos(count,pos): #accordingly whenever the mouse is clicked on an empty circle the image is placed as per the turn
	goat = pygame.image.load("/home/bvrit/Desktop/game/gig.bmp").convert()
	tiger = pygame.image.load("/home/bvrit/Desktop/game/tigs.bmp").convert()
	if pos != 0:
		for key in dic:
			if pos == key:
				center = dic[key]
				if count % 2 == 0:
					screen.blit(goat,[center[0] - 17, center[1] - 17])
					list[points.index(center)] = 'T'
				else:
					screen.blit(tiger,[center[0] - 17, center[1] - 17])
					list[points.index(center)] = 'G'
			
def drawPoints(radius): #for drawing all circles on the board
	for pt in points:
		pygame.draw.circle(screen,CIR_COL,pt,radius,0)

def boardPolygons(screen,LINE_COL,CIR_COL): #for drawing the path of the game
	pygame.draw.line(screen,LINE_COL,(100,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(650,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(100,700),(650,700),10)
	pygame.draw.line(screen,LINE_COL,(283,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(466,700),(375,50),10)
	pygame.draw.line(screen,LINE_COL,(50,560),(700,560),10)
	pygame.draw.line(screen,LINE_COL,(50,420),(700,420),10)
	pygame.draw.line(screen,LINE_COL,(50,280),(700,280),10)
	pygame.draw.line(screen,LINE_COL,(50,280),(50,560),10)
	pygame.draw.line(screen,LINE_COL,(700,280),(700,560),10)
	drawPoints(17)

def goatImage(): #to place all 15 goats on the board
	goat = pygame.image.load("/home/bvrit/Desktop/game/gig.bmp").convert()
	screen.blit(goat,[813,283])
	screen.blit(goat,[813,383])
	screen.blit(goat,[813,483])
	screen.blit(goat,[813,583])
	screen.blit(goat,[884,283])
	screen.blit(goat,[884,383])
	screen.blit(goat,[884,483])
	screen.blit(goat,[884,583])
	screen.blit(goat,[959,283])
	screen.blit(goat,[959,383])
	screen.blit(goat,[959,483])
	screen.blit(goat,[959,583])

def tigerImage(): #to place 3 tigers in there circles
	tiger = pygame.image.load("/home/bvrit/Desktop/game/tigs.bmp").convert()
	screen.blit(tiger,[360,40])
	screen.blit(tiger,[320,264])
	screen.blit(tiger,[390,264])
	list[0] = list[3] = list[4] = 't'

def load_background():
	background_image = pygame.image.load("/home/bvrit/Desktop/game/pic1.bmp").convert()
	screen.blit(background_image,[0,0])
	
	
size = [1000,950]
screen = pygame.display.set_mode(size)
pygame.display.set_caption("AADU PULI AATAM")
done = False
clock = pygame.time.Clock()
generatePossiblePoints()

while not done:
	tiger = pygame.image.load("/home/bvrit/Desktop/game/tigs.bmp").convert()
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			done = True
		elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
			pos = pygame.mouse.get_pos()
			count += 1
			print len(list)
			
	pygame.display.flip()
	screen.fill(MAROON)
	load_background()
	clock.tick(60)
	boardPolygons(screen,LINE_COL,CIR_COL)
	tigerImage()
	goatImage()
	changePos(count,pos)

	
pygame.quit()