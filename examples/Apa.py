import pygame

pygame.init()

BLACK = ( 0, 0, 0)
WHITE = ( 255, 255, 255)
RED = ( 255, 0, 0)

def playField():
	pygame.draw.polygon(screen, RED, [[350,35], [30,600], [670, 600]], 5)
	pygame.draw.polygon(screen, RED, [[30,450],[30,250], [670, 250],[670,450]], 5)
	pygame.draw.line(screen, RED, [30,350], [670, 350], 5)
	pygame.draw.line(screen, RED, [350,35], [247,600], 5)
	pygame.draw.line(screen, RED, [350,35], [493,600], 5)

	
size = (700, 700)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("My Game")


#background_image = pygame.image.load("/home/sowjanya/Pictures/website-background.jpg").convert()
#screen.blit(background_image, [0, 0])
# Unable to get the Background_image

done = False
clock = pygame.time.Clock()

while not done:
	for event in pygame.event.get(): 
		if event.type == pygame.QUIT: 
			done = True 
		screen.fill(WHITE)
		#Play - Field
		playField()

		pygame.display.flip()
		clock.tick(60)

pygame.quit()