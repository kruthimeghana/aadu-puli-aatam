import pygame
import random

pygame.init()

RED = (255,0,0)
WHITE = (255,255,255)
BLACK = (0,0,0)

size = [750,750]
screen = pygame.display.set_mode(size)

pygame.display.set_caption("MY GAME")


done = False

clock = pygame.time.Clock()
rec_y = 50
rec_x = 50
while not done:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			done = True

	pygame.display.flip()
	screen.fill(BLACK)
	clock.tick(60)
	for i in range(50):
		x = random.randrange(0, 750)
		y = random.randrange(0, 750)
	
	pygame.draw.rect(screen, WHITE, [rec_x, rec_y, 50, 50])
	pygame.draw.rect(screen, RED, [rec_x + 10, rec_y + 10 ,30, 30])
	rec_x += 1
	rec_y += 1
	pygame.draw.ellipse(screen,WHITE,[135,500,25,25])
	pygame.draw.ellipse(screen,WHITE,[123,520,50,50])
	pygame.draw.ellipse(screen,BLACK,[130,530,10,10])
	pygame.draw.ellipse(screen,BLACK,[145,530,10,10])
	pygame.draw.line(screen,RED,(135,550),(150,560),2)
	pygame.draw.ellipse(screen,WHITE,[100,565,100,100])
	

pygame.quit()
